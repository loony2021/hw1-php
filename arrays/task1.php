<?php
function minArrayNumb($array)
{
    $min = $array[0];
    $length = count($array);

    for ($i = 1; $i < $length; ++$i) {
        if ($array[$i] < $min) {
            $min = $array[$i];
        }
    }
    echo "Минимальное число массива ", json_encode($array), " = $min <br>";
}

minArrayNumb(array(1, 7, 8, -2, 3, 6, -4, 5));
minArrayNumb(array(1, 5, 421, 321, 6, 10, 1890));
minArrayNumb(array(123, -123, 4231, 5421, -1234));