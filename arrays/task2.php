<?php
function maxArrayNumb($array)
{
    $max = $array[0];
    $length = count($array);

    for ($i = 1; $i < $length; ++$i) {
        if ($array[$i] > $max) {
            $max = $array[$i];
        }
    }

    echo "Максимальное число массива ", json_encode($array), " = $max <br>";
}

maxArrayNumb(array(1, 7, 8, -2, 3, 6, -4, 5));
maxArrayNumb(array(1, 5, 421, 321, 6, 10, 1890));
maxArrayNumb(array(123, -123, 4231, 5421, -1234));