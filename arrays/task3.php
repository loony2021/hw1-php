<?php
function minIndex($array)
{
    $min = $array[0];
    $index = 0;
    $length = count($array);

    for ($i = 1; $i < $length; ++$i) {
        if ($array[$i] < $min) {
            $min = $array[$i];
            $index = $i;
        }
    }
    echo "Индекс минимального число массива ", json_encode($array), " = $index <br>";
}

minIndex(array(1, 7, 8, -2, 3, 6, -4, 5));
minIndex(array(12, -12, 42, 92, 19));
minIndex(array(-100, -54, 12, 521, 81));