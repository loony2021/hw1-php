<?php
function maxIndex($array)
{
    $length = count($array);
    $max = $array[0];
    $index = 0;

    for ($i = 1; $i < $length; ++$i) {
        if ($array[$i] > $max) {
            $max = $array[$i];
            $index = $i;
        }
    }

    echo "Индекс максимального число массива ", json_encode($array), " = $index <br>";
}

maxIndex(array(1, 7, 8, -2, 3, 6, -4, 5));
maxIndex(array(12, -12, 42, 92, 19));
maxIndex(array(-100, -54, 12, 521, 81));