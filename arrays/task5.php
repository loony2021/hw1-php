<?php
function maxIndex($array)
{
    $length = count($array);
    $sum = 0;

    for ($i = 0; $i < $length; $i++) {
        if ($i % 2 != 0) {
            $sum += $array[$i];
        }
    }

    echo "Сумма элементов массива с нечетными индексами ", json_encode($array), " = $sum <br>";
}

maxIndex(array(1, 7, 8, -2, 3, 6, -4, 5));
maxIndex(array(12, -12, 42, 92, 19));
maxIndex(array(-100, -54, 12, 521, 81));
?>