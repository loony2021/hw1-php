<?php
function countOddNumber($array)
{
$length = count($array);
$num = 0;

for ($i = 0; $i < $length; $i++) {
    if ($array[$i] % 2 != 0) {
        $num++;
        }
}

echo "Сумма элементов массива с нечетными индексами ", json_encode($array), " = $num <br>";
}
countOddNumber(array(1, 7, 8, -2, 3, 6, -4, 5));
countOddNumber(array(2, 4, 6, -2, -4, -6, 8, -8));
countOddNumber(array(1, 3, 5, 7, 9, 11));
