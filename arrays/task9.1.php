<?php
function sortArray($array)
{
    echo "Сортировка массива ", json_encode($array);
    $length = count($array);

    for ($i = 0; $i < $length - 1; $i++) {
        $min = $i;
        for ($j = $i + 1; $j < $length; $j++) {
            if ($array[$j] < $array[$min]) {
                $min = $j;
            }
        }

        $temp = $array[$i];
        $array[$i] = $array[$min];
        $array[$min] = $temp;
    }

    $array = json_encode($array);

    echo " = $array <br>";
}

sortArray(array(1, 7, 8, -2, 3, 6, -4, 5));
sortArray(array(2, 4, 6, -2, -4, -6, 8, -8));
sortArray(array(1, 3, 5, 7, 9, 11));