<?php
function bubbleSort($array)
{
    echo "Сортировка массива ", json_encode($array);
    $n = count($array);
    for ($i = 0; $i < $n; $i++) {
        $swapped = False;
        for ($j = 0; $j < $n - $i - 1; $j++) {
            if ($array[$j] > $array[$j + 1]) {
                $t = $array[$j];
                $array[$j] = $array[$j + 1];
                $array[$j + 1] = $t;
                $swapped = True;
            }
        }
        if ($swapped == False)
            break;
    }

$array = json_encode($array);

echo " = $array <br>";
}

bubbleSort(array(1, 7, 8, -2, 3, 6, -4, 5));
bubbleSort(array(2, 4, 6, -2, -4, -6, 8, -8));
bubbleSort(array(1, 3, 5, 7, 9, 11));