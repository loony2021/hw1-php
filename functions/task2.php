<?php
$ones = array('', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять');
$tens = array('', '', 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто');
$teens = array('десять', 'одинадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать');
$hundreds = array('', 'сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот');

function convert_hundreds($num)
{
    global $hundreds;
    if ($num > 99) {
        $index = intval(floor($num / 100));
        return $hundreds[$index] . " " . convert_tens($num % 100);
    } else {
        return convert_tens($num);
    }
}

function convert_tens($num)
{
    global $ones, $tens, $teens;
    if ($num < 10) {
        return $ones[$num];
    } else if ($num >= 10 && $num < 20) {
        return $teens[$num - 10];
    } else {
        $index = intval(floor($num / 10));
        return $tens[$index] . " " . $ones[$num % 10];
    }
}

function convertToLetters($num)
{
    if ($num == 0) {
        return "ноль";
    }
    if ($num < 0 || $num > 999) {
        return "число не входит в заданный диапазон 0-999.";
    } else echo "$num : ", convert_hundreds($num), "<br>";
}

convertToLetters(123);
convertToLetters(321);
convertToLetters(666);
?>
