<?php
function xy($x1, $y1, $x2, $y2)
{
    $i = pow(($x2 - $x1), 2);
    $j = pow(($y2 - $y1), 2);
    $result = (sqrt($i + $j));
    $result = round($result, 2);
    echo "1 : [$x1, $y1], 2 : [$x2, $y2] = $result <br>";
}

xy(1, 2, 3, 4);
xy(6, 6, 2, 2);
xy(6, 1, 8, 3);
?>