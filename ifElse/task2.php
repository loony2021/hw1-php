<?php
function xy($x, $y)
{
    echo "Точка x = $x, y = $y пренадлежит ";
    if ($x > 0 && $y > 0) {
        echo "1 четверть<br>";
    } else if ($x < 0 && $y > 0) {
        echo "2 четверть<br>";
    } else if ($x < 0 && $y < 0) {
        echo "3 четверть<br>";;
    } else if ($x > 0 && $y < 0) {
        echo "4 четверть<br>";
    } else if ($x == 0 && $y == 0) {
        echo "Начало координат<br>";
    } else if ($x != 0 && $y == 0) {
        echo "На оси У<br>";
    } else if ($x == 0 && $y != 0) {
        echo "На оси Х<br>";
    }
}

xy(4, 5);
xy(-4, 5);
xy(-4, -5);