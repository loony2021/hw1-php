<?php
function expre($a, $b, $c)
{
    $mul = $a * $b * $c + 3;
    $sum = $a + $b + $c + 3;
    if ($mul > $sum) {
        echo "Произведению производных $a, $b $c = $mul <br>";
    } else if ($mul < $sum) {
        echo "Сумма производных $a, $b $c = $sum <br>";
    } else if ($mul == $sum) {
        echo "Сумма $a, $b $c равна произведению <br>";
    }
}

expre(1, 2, 3);
expre(6, 3, 1);
expre(9, 5, 2);
expre(0, 5, 1);