<?php

function mark($rating)
{
    echo "Оценка студента $rating - ";
    if ($rating >= 0 && $rating <= 19) {
        echo "Оценка F <br>";
    } else if ($rating >= 20 && $rating <= 39) {
        echo "Оценка E <br>";
    } else if ($rating >= 40 && $rating <= 59) {
        echo "Оценка D <br>";
    } else if ($rating >= 60 && $rating <= 74) {
        echo "Оценка C <br>";
    } else if ($rating >= 75 && $rating <= 89) {
        echo "Оценка B <br>";
    } else if ($rating >= 90 && $rating <= 100) {
        echo "Оценка A <br>";
    } else if ($rating < 0 || $rating > 100) {
        echo "Рейтинговый балл введен неправильно <br>";
    }
}

mark(60);
mark(69);
mark(74);
mark(85);
mark(90);