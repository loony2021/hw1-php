<?php
function radix($num)
{
    echo "Корень $num - ";
    $sqrt = 0;
    for ($i = 1; $i <= $num; $num -= $i, $i += 2) {
        $sqrt++;
    }

    echo "$sqrt <br>";
}

radix(60);
radix(41);
radix(78);
radix(100);